const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const planIt = require('./planit-handler');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.get('/', (req, res) => res.send('Hello World!'));

app.post('/planit' , (req, res) => {

  planIt.process(req, res);


});

app.listen(3000, () => {
  console.log('listening on port 3000');
});